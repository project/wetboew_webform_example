Author: joseph.olstad -> https://drupal.org/u/joseph.olstad.

As of this writing, the wetboew serverside validation javascript component conflicts with clientside_validation_jquery
(module or project).

If you want the javascript to work correctly make sure clientside_validation_jquery is not enabled or
make sure that a working patch of some kind (related) is applied to clientside_validation_jquery or this module.
