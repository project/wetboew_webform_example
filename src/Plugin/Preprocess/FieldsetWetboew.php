<?php

namespace Drupal\wetboew_webform_example\Plugin\Preprocess;

use Drupal\bootstrap\Utility\Element;
use Drupal\bootstrap\Utility\Variables;

/**
 * Pre-processes variables for the "fieldset" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("fieldset")
 */
class FieldsetWetboew extends PreprocessBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  public function preprocessElement(Element $element, Variables $variables) {
    // Map the element properties.
    $variables->map(['attributes', 'errors', 'required', 'legend', 'children', 'prefix', 'suffix', 'description']);

    // Preprocess attributes.
    $this->preprocessAttributes();
  }

}
